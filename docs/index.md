# ITT Internship and Final Project Info

Information about internship, final project, exams and report are found here.


## General Internship

In order to complete, successfully the fourth semester of ITT. Students are required to have a three calendar months internship placing. The internship is recommenced to start on with the start of the fourth semester. That is 01/01/20xx. With xx been the year of study that the students is fourth semester.

## General Final Report

In order to complete, successfully the fourth semester of ITT and receive the graduation certificate. The student needs to successfully pass the final exam. The exam is around a project the student has worked on. The project can be either independent, a research project with one of the teachers or with a company. The project requires a report, written in an academic way.


## Exams

Exam details are posted in the exam catalog. But in order for the student to be allowed to participate in the Final Report exam. The student is required to pass the Internship exam.



## Other Notes

Students can express a wish to have a specific supervisor. In general this is accommodated to first come, first gets basis.

In some extreme cases can start their internships later. For this you will require a dispensation. As the exam needs to be moved accordingly. Reasons for dispensation are for example, illness

Additional help can be provided by the internship coordinator or the student counselor.


Students are advised to have an internship before Christmas so that paperwork can be processed. Furthermore students that wish to go abroad for the duration of their internship and final project. Have the option for Erasmus, this will require additional paperwork and the process should start sooner. Ideally 2 to 3 months before internship start date. 
