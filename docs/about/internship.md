# Internship

Information about your Internship as part of ITT Education is found below. Remember that nothing is better than asking a question when confused.


# The Purpose with Internship and Main Subject

The purpose of the Internship and the Main Subject is to give you a possibility to match you education to interests and future career by working with relevant and concrete problems and to get insight in related business functions.

In the final exam project the student will document his or her ability to analyze and work with complex, real life problems, in relation to a concrete task within the IT area. The main exam project must include key topics from the education.

See study curriculum for details. This include both the national and the institution specific part. The curriculum is found on UCL.dk under the ITT education.

During the internship, you must be associated with one or more private or public companies in Denmark or abroad. During the internship, you must work with one or more issues with in the field of information technology. Topics and tasks needs to be verified with our supervisor or the internship coordinator.

The internship and the main task can be carried out fully or partially in the company location. The final project can be carried out at the companies office space or at the academics address. This is something that needs to be made clear to the company.

It is not required that the main task performed for the same company, but it will often be beneficial for both the company and the student.


# Who finds the Company?

It is primarily the students task, to find a suitable company for the Internship. The Internship Coordinator on the education will assist you with guidance, supervision and suggestions, to find a suitable company.

When looking for a suitable company it is important for the student to consider how the Internship and Main Project with the company can assist in:

1. Giving you relevant and interesting Internship
2. Give you a deeper insight in the core business and understanding of the company
3. Give you a better understanding of the theory behind your education
4. Give you a better understanding regarding the connection between theory and practice.

It is important the company knows that the student receives SU for the duration of the internship.

In case the company wishes for a monetary compensation. They are allowed to pay up to 3000 danish kroner. Any amount over this will affect your SU which will have to be stopped. In general, ask the study service or your internship coordinator you have any questions related to SU or need clarifications.

It is suggested to the companies that the Interns to combine the Internship period (3 calendar months) and the Main Project period (10 weeks) so the students stays the whole time (20 weeks or six months, a full semester) at the company’s premises. When you finalize the Internship period and have learned a lot about the everyday work, the competition, the customers and the market, you are much better suited to finish you Main Project Report for the company.

For internship outside Denmark the same rules apply. The UCL website has helpful information for going abroad.

# Internship


## Registering

The registration of an internship is done via [praktikportal](https://www.ucpraktikportal.dk/account/signin/ucl/stud?ReturnUrl=%2F). There are two parts. The first is registration in Denmark that needs to be done using the portal.
For internships abroad you will also need to register using the [international office form](https://www.ucl.dk/for-studerende/under-studiet/udlandsophold). Please note that for internships aboard you need to fill out both. Pay attention that a lot of the information is recursive.

## Time and Duration 

The Internship period is equal to 15 ECTS points. The Internship period is in the last semester of the education. Basically there is no winter or autumn vacation, unless otherwise agreed by the conclusion of the internship agreement (you may start one week earlier). Any public holidays during the period will be kept free.

Main Project and Report process must also correspond to 15 ECTS points, and is a natural extension of the Internship period. It is expected that you work full-time with your main Project.

## Your Economy

Students from EU, will have to make sure that they follow the latest rules about SU and work in Denmark. For more information contact the SU office

Students from outside the EU should contact the immigration services if the company has head hunted them. For more information contact the internship coordinator

In case you are taking internship outside Denmark, in most cases you will be able to take your SU with you. You should also consider Erasmus funds.


## Insurance

The Industrial Injury Board in Denmark has confirmed that academy students and professional bachelor students are included in the Order nr. 937 af 26/11-2003 regarding Industrial Injury insurance for education-seeking people. § 1, nr. 12. This mean that you, as an academic student or PBa student, is insured against injuries in connection with your Internship, because the Internship is part of your education. The Internship must be acknowledged by the Educational Institution, in this case UCL. You will, as a student, be insured by the Internship company’s Industrial Injury Insurance.
Taking an internship abroad comes with the risk that they company may not have an work  inside insurance. In this case you need to come to an agreement even with your insurance company or the company.

## Organization of the Internship

For the duration of the internship you are considered employee of the company and student the same time. It is paramount that you send a status email to your supervisor every week, and updated time plan. Students should work on a time plan with the company, task to be accomplish, milestones, etc. This time plan should be shared with the supervisor, students should hold count of his or her work with accordance to the time plan. Status of work should be presented to the UCL supervisor every week, in a simple, form. Tasks to do this week, tasks accomplish, tasks for next week. You are also encouraged to contact your supervisor and actively share knowledge and challenge the status quo.

## Documentation During the Internship

The documentation that needs to be produced during the internship, depends on an arrangement between the student the company and the UCL supervisor. The following are some suggestions:

1. Weekly status reports, in the form of an email, pdf document, or a day log. This can change according to your supervisor.
2. Gitlab project, with work, milestones and status.
3. Google hangout meeting/Zoom meetings/On site meetings if needed
4. Any other arrangement.


## Exam and Submission

At the end of the your internship you will need to submit a poster and make a presentation. Please see relevant curriculum at the education's [home page](https://www.ucl.dk) for details on the exam for. Furthermore details will be posted via wiseflow as they become relevant.

### Structure of Poster

The poster should cover all the technical aspects that where covered during the internship. Other details should be clarified with the relevant supervisor and match the curriculum of the education.

## Evaluation

Evaluation is done based on the 7-scale danish system.

## Re-Test/Exam

If you do not pass the exam, you must participate in a re-exam. As with other exams, you are entitled to two (2) re-exams. The foundation for a re-exam is based on a professional evaluation by your UCL supervisor, based on the following: 

1. If you fail due to the lack of participation in the internship, a new internship can be established. The details have to be agreed with your UCL internship coordinator.
2. If you fail due to the assessment/evaluation poster presentation. New exam will be arranged and extra consulting can be provided.











