# Report Template

Information about writting a good report

## Problem statement

A short and descriptive statement of what the project will be about. 

Examples:

1. "Evaluation of environmental data for data centers using zigbee green"

2. "Chemical analysis and health risks from pesticides in drinking water"

3. "IoT topology off wifi based indoor tracking"

4. "Game creation using virtual hardware - hardware optimization for game acceleration"

The project statement needs to be approved by the students supervisor.


## Research

In this phase research of technology existing technology needs to be made. It is a good idea that the student builds up case and knowledge of how to implement and build up his or her case.


## Notes

Building of knowledge portfolio. Setting up a gitlab, creating notes from the internship period.


## Report

The final exam is Report based. The report will be the baseline for your exam. A very good report will give the student a higher starting point. Remember the external sensor does not know you. His or hers first impression is your report. A report not well presented will lead to the student missing on getting credit for his or her work.

### Table of Contented

If you are writing in MS WORD or something equivalent this is advised to be auto-generated. As changed in the report will automatically change and format the ToC.


### Introduction

Introduce the project, the problem statement, the background of the case, initial ideals, values, expectations, set up, ect.


### Main Topic

Here students will go into details about their solutions

#### Topic 1

For example hardware set up and information details of set up and testing. This can be separated in several subtopics. For example Zigbee, LORA, Zwave, etc

#### Topic 2

For example software. Here the student can evaluated and present his or her work with respect to software

### Discussion


Results are presented here. Evaluation is done here and suggestions for future work or improvements.

### Conclusion


Remember conclusion is linked to introduction. Here everything that was presented in the introduction has to be touched and concluded upon based on the results from Discussion.

### Literature

List of literature list and references

### Appendix

Appedix of code, too big images for example business plan, etc







